/*
 * 带元素切换Slider类
 * 正常的焦点图滚动，可自定义单幅图上的切入元素
 * 
 * 可任意转载使用
 * 望保留以下版权
 *
 *    Developed by COoL  |  QQ:176512025  |  Email:176512025@qq.com
 *       _______     _______             _
 *      /  ______\  /  ___  `\    ___   / \
 *     /\  \       /\  \   \  \  / __`\/\  \
 *     \ \  \______\ \  \___\  \/\ \_\ \ \  \_______
 *      \ \________/\ \________/\ \____/\ \________/
 *       \/_______/  \/_______/  \/___/  \/_______/
 *
 */

var CoolBanner = function (containerid, width, height, datas, extoptions) {
    if (typeof CoolBanner._initialized == "undefined") {
        CoolBanner.prototype.init = function(){
            this.container.css({width:this.width,height:this.height,overflow:'hidden',position:'relative'});
            this.container.data('bannerid',this.id);
            this.container.append(this.slider);
            this.slider.css({width:this.datas.length+'00%',height:'100%',overflow:'hidden',position:'absolute',left:'0px',top:'0px'});
            this.items=new Array();
            this.points=new Array();
            for(var i=0;i<this.datas.length;i++){
                this.items[i]=$(document.createElement('div'));
                this.items[i].css({float:'left',width:100/this.datas.length+'%',height:'100%',overflow:'hidden',position:'relative',backgroundImage:"url("+this.datas[i].mainimg+")",backgroundSize:'auto 100%',backgroundRepeat:'no-repeat',backgroundPosition:'center',backgroundColor:this.datas[i].bgcolor});
                this.slider.append(this.items[i]);
                this.points[i]=$(document.createElement('div'));
                this.points[i].css({position:'absolute',cursor:'pointer',bottom:'10px',right:((this.datas.length-i)*(this.pointsize+4))+'px',width:this.pointsize+'px',height:this.pointsize+'px',backgroundColor:this.pointcoloroff,borderRadius:this.pointradius+'px'});
                this.container.append(this.points[i]);
                this.points[i].click(function(){
                    eval($(this).parent().data('bannerid')+'.setnext('+($(this).prevAll().length-1)+')');
                });
            }
            this.current=0;
            this.goto(this.current);
        };
        CoolBanner.prototype.setnext = function(no){
            this.current=no-1;
        };
        CoolBanner.prototype.goto = function(no){
            this.slider.animate({left:-1*no+'00%'},500);
            this.points[no].css({backgroundColor:this.pointcoloron}).siblings().css({backgroundColor:this.pointcoloroff});
            setTimeout(this.id+'.doshow()',500);
        };
        CoolBanner.prototype.goprev = function(){
            this.current=(this.current-1<0)?this.datas.length-1:this.current-1;
            this.goto(this.current);
        };
        CoolBanner.prototype.gonext = function(){
            this.current=(this.current+1>=this.datas.length)?0:this.current+1;
            this.goto(this.current);
        };
        CoolBanner.prototype.doshow = function(){
            //console.log(this.current);
            for(var i=0;i<this.datas.length;i++){
                this.items[i].empty();
            }
            var doms=this.datas[this.current].elements;
            for(var i=0;i<doms.length;i++){
                var dom=$(document.createElement('div'));
                dom.html(doms[i].html);
                dom.css({position:'absolute',left:'0px',top:'0px'});
                dom.css(doms[i].begincss);
                this.items[this.current].append(dom);
                var tmpr=Math.floor(Math.random()*100);
                var tmpDom = "obj_"+new Date().getTime()+tmpr+'_dom';
                var tmpCss = "obj_"+new Date().getTime()+tmpr+'_css';
                var tmpSpeed = "obj_"+new Date().getTime()+tmpr+'_speed';
                //这一句eval就相当于在obj_xxx(这是外部的变量)=this(对象本身)
                eval(tmpDom+"=dom");
                eval(tmpCss+"=doms[i].endcss");
                eval(tmpSpeed+"=doms[i].speed");
                setTimeout(tmpDom+'.animate('+tmpCss+','+tmpSpeed+')',doms[i].delay);
            }
            setTimeout(this.id+'.gonext()',this.datas[this.current].stay);
        };
        
        CoolBanner._initialized = true;
    }

    //产生一个随机的字符串用做ID,这里用"obj_"加时间戳的方式
    this.id = "obj_"+new Date().getTime();
    //这一句eval就相当于在obj_xxx(这是外部的变量)=this(对象本身)
    eval(this.id+"=this");
    
    this.container=$('#'+containerid);//定义容器ID
    this.slider=$(document.createElement('div'));
    this.width = /^\d+$/.test(width) ? width+'px' : width;//定义宽度
    this.height = /^\d+$/.test(height) ? height+'px' : height;//定义高度
    this.extoptions=extoptions;
    var options={
        pointsize: 0,
        pointradius: 5,
        pointcoloron: '#ff6000',
        pointcoloroff: '#ffffff'
    };
    $.extend(options, extoptions);
    this.pointsize=options.pointsize;//定义按钮大小
    this.pointradius=options.pointradius;//定义按钮大小
    this.pointcoloron=options.pointcoloron;//定义按钮激活颜色
    this.pointcoloroff=options.pointcoloroff;//定义按钮失效颜色
    this.datas=datas;//定义数据
    this.init();
};